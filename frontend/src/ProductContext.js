import React from "react";

// Create a Context Object
// A Context Object as the name states is a data type of an object that can be used to store information that can be shared within the app.
// The Context Object is a different approach to passing information between components and allows easier access avoiding the use of props

// Using the createContext from react we can create a context in our app
// We contain the context created in our UserContext variable
// We named it UserContext simply because this context will contain the information of our user.
const ProductContext = React.createContext();

// The Provider component allows other components to consume/use the context object and supply the necessary information needed to the context object
export const ProductProvider = ProductContext.Provider;

export default ProductContext;
