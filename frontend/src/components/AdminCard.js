import { useContext, useState } from "react";
import { Card, Button } from "react-bootstrap";
// import {Link}                from 'react-router-dom';
import Modal from "react-bootstrap/Modal";
// import {useParams}           from 'react-router-dom';
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
export default function ProductCard({ productProp, refresh }) {
  const { name, description, price, _id, isActive, image } = productProp;
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);
  const [newName, setName1] = useState("");
  const [newDescription, setDescription1] = useState("");
  const [newPrice, setPrice1] = useState("");
  const [newImage, setNewImage] = useState();
  const navigate = useNavigate();

  //deleteProduct Function
  const deleteProduct = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/deleteProduct/${_id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-type": "application/json"
      }
    })
      .then((res) => res.json())
      .then((data) => {
        //TODO:TESTING
        // console.log(data);
        Swal.fire({
          title: "Product Deleted Successfuly",
          icon: "success"
        });
        // navigate("/courses");
        refresh();
      });
  };
  //end deleteProduction function ////////////////////

  const archiveProduct = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${_id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-type": "application/json"
      }
    })
      .then((res) => res.json())
      .then((data) => {
        //TODO:TESTING
        // console.log(data);
        Swal.fire({
          title: "Product Archived Successfuly",
          icon: "success"
        });
        // navigate("/courses");
        refresh();
      });
  };

  //activateProduct //////////////////////////////
  const activateProduct = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/activateProduct/${_id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-type": "application/json"
      }
    })
      .then((res) => res.json())
      .then((data) => {
        //TODO:TESTING
        // console.log(data);
        Swal.fire({
          title: "Product Activated",
          icon: "success"
        });
        // navigate("/courses");
        refresh();
      });
  };

  //update product function //////////////////////////
  const updateProduct = (token) => {
    //TODO:TESTING
    // console.log("here");
    // console.log(name);
    // console.log(description);
    // console.log(_id);
    fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${_id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        name: `${newName}`,
        description: `${newDescription}`,
        price: `${newPrice}`,
        image: `${newImage}`
      })
    })
      .then((res) => res.json())
      .then((data) => {
        //TODO:TESTING
        // console.log("Im here");
        // console.log(data);
        Swal.fire({
          title: "Product Updated Successfuly",
          icon: "success"
        });
        // navigate("/courses");

        refresh();
        handleClose(false);
      });
  };
  //end updateProduct function

  return (
    <>
      <Card className="CourseCard p-3 mb-3 w-50">
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price</Card.Subtitle>
          <Card.Text>₱ {price}</Card.Text>
        </Card.Body>

        {isActive ? (
          <>
            <Button variant="primary" className="w-50 mb-2">
              Edit
            </Button>
            <Button
              variant="danger"
              className="w-50 mb-2"
              onClick={deleteProduct}
            >
              Delete
            </Button>
            <Button
              variant="primary"
              className="w-50 mb-2"
              onClick={archiveProduct}
            >
              Archive
            </Button>
          </>
        ) : (
          <>
            <Button
              variant="primary"
              className="w-50 mb-2"
              onClick={handleShow}
            >
              Edit
            </Button>
            <Button variant="danger" className="w-50 mb-2">
              Delete
            </Button>
            <Button
              variant="secondary"
              className="w-50 mb-2"
              onClick={activateProduct}
            >
              Set as active
            </Button>
          </>
        )}
      </Card>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add/Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                defaultValue={name}
                onChange={(e) => setName1(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Price"
                defaultValue={price}
                onChange={(e) => setPrice1(e.target.value)}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                type="number"
                placeholder="Description"
                defaultValue={description}
                onChange={(e) => setDescription1(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="productImage">
              <Form.Label>Product Image URL</Form.Label>
              <Form.Control
                type="text"
                name="productImage"
                value={image}
                onChange={(e) => setNewImage(e.target.value)}
                required
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={updateProduct}>
            Update Product
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
