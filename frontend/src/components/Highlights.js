import { Row, Col, Card, Button } from "react-bootstrap";
import { Placeholder } from "react-bootstrap";
export default function Highlights() {
  return (
    <div className="d-flex justify-content-around">
      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://fishingbooker.com/blog/media/2020/07/Rods-in-a-store.jpg"
        />
        <Card.Body>
          <Card.Title>Fishing Rods</Card.Title>
          <Card.Text>
            Introducing our premium fishing rod, designed to enhance your
            angling experience and reel in unforgettable catches. Crafted with
            meticulous attention to detail and utilizing cutting-edge materials,
            this fishing rod combines strength, sensitivity, and versatility,
            making it an ideal companion for anglers of all skill levels.
          </Card.Text>
        </Card.Body>
      </Card>

      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://u7q2x7c9.stackpathcdn.com/photos/23/55/357012_6638_XL.jpg"
        />
        <Card.Body>
          <Card.Title>Fishing Reels</Card.Title>
          <Card.Text>
            Introducing our high-performance fishing reel, engineered to elevate
            your fishing experience to new heights. Meticulously designed and
            packed with advanced features, this reel is a game-changer in the
            world of angling. From its robust construction to its smooth
            operation, every aspect has been optimized to deliver unparalleled
            performance.
          </Card.Text>
        </Card.Body>
      </Card>

      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://www.fishingsuperstore.com.au/assets/thumbL/AUS32.jpg?20220315192144"
        />
        <Card.Body>
          <Card.Title>Jigs and Lures</Card.Title>
          <Card.Text>
            Introducing our collection of premium fishing lures and jigs,
            meticulously crafted to entice and outsmart even the most elusive
            fish species. Designed by avid anglers for anglers, these lures are
            the ultimate tools for enhancing your fishing success and reeling in
            trophy catches. Each lure and jig is carefully engineered to mimic
            natural prey, provoking instinctive strikes and maximizing your
            chances of landing the big one.
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}
