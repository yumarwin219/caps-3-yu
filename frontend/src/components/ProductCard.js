import { useState, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function CourseCard({ productProp }) {
  const { user } = useContext(UserContext);
  const { name, description, price, _id, image } = productProp;

  return (
    <Row className="mt-3 mb-3" id="prodRow">
      <Col className="" id="prodCol">
        <Card id="prodCard" className=" mx-auto" style={{ width: "100%" }}>
          <Card.Img variant="top" src={image} alt={name} />
          <Card.Body id="prodBod">
            <Card.Title>{name}</Card.Title>
            {/* <Card.Text>{description}</Card.Text> */}
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>₱ {price}</Card.Text>
            {user._id !== null ? (
              <>
                <Link
                  id="prodDetails"
                  className="btn btn-primary "
                  to={`/productView/${_id}`}
                >
                  View Details
                </Link>
              </>
            ) : (
              <>
                <Link className="btn btn-primary" to={`/login`}>
                  Details
                </Link>
              </>
            )}
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
