import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import "../App.css";
export default function Home() {
  return (
    <>
      <div id="wholebg">
        <Banner />
      </div>
      <div id="btn">
        <Button id="btn" variant="primary" as={Link} to="/products">
          Shop now
        </Button>
      </div>
      {/* <Highlights /> */}
    </>
  );
}
