import coursesData from "../data/coursesData";
import ProductCard from "../components/ProductCard";
import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";

export default function Courses() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);

        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  return (
    <>
      <Row>
        {products.map((product) => (
          <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
            {product}
          </Col>
        ))}
      </Row>
    </>
  );
}
