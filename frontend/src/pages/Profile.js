import { useState, useEffect, useContext } from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Profile() {
  const { user } = useContext(UserContext);

  // allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
  const history = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");

  const { userId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details/${userId}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setMobileNo(data.mobileNo);
      });
  }, [firstName, lastName, email, mobileNo]);

  return (
    <Container className="mt-5">
      <Row>
        <Col>
          <Card id="profileCard" className="p-2">
            <Card.Body>
              <h3 className="pb-3">Profile:</h3>
              <Card.Title className="mb-3">
                Name: {lastName}, {firstName}
              </Card.Title>
              <Card.Title className="mb-3">Email: {email}</Card.Title>
              <Card.Title className="mb-3">
                Mobile Number: {mobileNo}
              </Card.Title>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
